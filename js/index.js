$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("[data-toggle='popover']").popover();
    });
    $('.carousel').carousel({
      interval:2000
    });
    $('#contacto').on('show.bs.modal',function(e){ // el on se utiliza para suscribir eventos
        console.log("El modal se está mostrando" , e );
        $('#contactobtn').removeClass('btn-outline-success');
        $('#contactobtn').addClass('btn-primary');
        $('#contactobtn').prop('disabled',true);
    });
    $('#contacto').on('shown.bs.modal',function(e){ // el on se utiliza para suscribir eventos
        console.log("El mensaje de modal contacto se mostró" , e );
    });
    $('#contacto').on('hide.bs.modal',function(e){ // el on se utiliza para suscribir eventos
        console.log("El modal se oculta" , e );
    });
    $('#contacto').on('hidden.bs.modal',function(e){ // el on se utiliza para suscribir eventos
        console.log("El modal de contacto se ha ocultado" , e );
        $('#contactobtn').prop('disabled',false);
        $('#contactobtn').removeClass('btn-primary');
        $('#contactobtn').addClass('btn-outline-success');
    });